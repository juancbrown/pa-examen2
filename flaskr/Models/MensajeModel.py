import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db


class MensajeModel:

    def VerMensaje(self, parametros):
        db = get_db()
        mensaje = db.execute(
            'SELECT * FROM mensajes WHERE id = ?', (parametros.get("id_mensaje"))
        ).fetchall()
        return mensaje

    def VerMensajes(self):
        db = get_db()
        mensaje = db.execute(
            'SELECT * FROM mensajes'
        ).fetchall()
        return mensaje

    def CrearMensaje(self, parametros):
        print(parametros)
        db = get_db()
        db.execute(
            'INSERT INTO mensajes (id_emisor, id_receptor, titulo, mensaje, enviado, recibido)'
            ' VALUES (?, ?, ?, ?, ?, ?)',
            (parametros.get("id_emisor"), parametros.get("id_receptor"), parametros.get("titulo"),
             parametros.get("mensaje"), parametros.get("enviado"), parametros.get("recibido"))
        )
        db.commit()
        return 'ok'

    def MensajesEmisor(self, parametros):
        db = get_db()
        mensajes = db.execute(
            'SELECT * FROM mensajes WHERE id_emisor = ?', (parametros.get("id_emisor"))
        ).fetchall()
        return mensajes

    def MensajesReceptor(self, parametros):
        db = get_db()
        mensajes = db.execute(
            'SELECT * FROM mensajes WHERE id_emisor = ?', (parametros.get("id_receptor"))
        ).fetchall()
        return mensajes

    def EliminarMensaje(self, parametros):
        db = get_db()
        db.execute(
            'DELETE FROM mensajes WHERE id = ?', (parametros.get("id_eliminar"))
        )
        db.commit()
        return 'Mensaje eliminado.'

    def ActualizarMensaje(self,parametros):
        #
        db = get_db()
        db.execute(
            'UPDATE mensajes SET id_emisor = ?, id_receptor = ?, titulo = ?, mensaje = ?, enviado = ?, recibido = ?'
            ' WHERE id = ?',
            (parametros.get("id_emisor"), parametros.get("id_receptor"), parametros.get("titulo"),
             parametros.get("mensaje"), parametros.get("enviado"), parametros.get("recibido"), parametros.get("id"))
        )
        db.commit()
        mensaje = db.execute(
            'SELECT * FROM mensajes WHERE id = ?', (parametros.get("id"))
        ).fetchall()
        return mensaje


    def UsuarioExiste(self, id_usuario):
        # Codigo para validar usuario
        return True

    def RegistrarAccion(self, id_mensaje, fecha, accion):
        # Registrar entrada en bitacora
        return True

    def VerAccionesMensaje(self):
        pass

