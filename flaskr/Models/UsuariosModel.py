import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

class UsuariosModel:

    def VerUsuarios(self):
        print('En modelo usuarios')
        db = get_db()
        usuarios = db.execute(
            'SELECT * FROM user'
        ).fetchall()
        print('Usuarios leidos')
        return usuarios

    def VerUsuario(self, parametros):
        print('En modelo usuarios')
        db = get_db()
        usuarios = db.execute(
            'SELECT * FROM user WHERE id = ?',(parametros.get("id_usuario"))
        ).fetchall()
        print('Usuarios leidos')
        return usuarios

    def UsuarioExiste(self, parametros):
        print('')