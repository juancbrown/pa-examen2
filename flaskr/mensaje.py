import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

from flaskr.Controllers.MensajesController import MensajesController
from flaskr.Controllers.UsuariosController import UsuariosController
from flaskr.Views.ErrorView import ErrorView


#if __name__ == '__main__':

bp = Blueprint('mensaje', __name__)


@bp.route('/api/mensajes/ver', methods=['GET', 'POST'])
def apiVerMensaje():
    try:
        print(request)
        controlador = MensajesController()
        respuesta = controlador.VerMensaje(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        return error.Render(str(e))

@bp.route('/api/mensajes/todos', methods=['GET', 'POST'])
def apiVerMensajesTodos():
    try:
        print(request)
        controlador = MensajesController()
        respuesta = controlador.VerMensajes(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        return error.Render(str(e))


@bp.route('/api/mensajes/emisor', methods=['GET', 'POST'])
def apiVerMensajesEmisor():
    try:
        print(request)
        controlador = MensajesController()
        respuesta = controlador.VerMensajesEmisor(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        return error.Render(str(e))

@bp.route('/api/mensajes/receptor', methods=['GET', 'POST'])
def apiVerMensajesReceptor():
    try:
        print(request)
        controlador = MensajesController()
        respuesta = controlador.VerMensajesReceptor(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        return error.Render(str(e))


@bp.route('/api/mensajes/nuevo', methods=['GET', 'POST'])
def apiCrear():
    try:
        controlador = MensajesController()
        print(request)
        respuesta = controlador.CrearMensaje(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        mensaje = 'Error en la aplicacion'
        return error.Render(mensaje)


@bp.route('/api/mensajes/eliminar', methods=['GET', 'POST'])
def apiMensajesEliminar():
    try:
        controlador = MensajesController()
        respuesta = controlador.EliminarMensaje(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        mensaje = 'Error en la aplicacion'
        return error.Render(mensaje)


@bp.route('/api/mensajes/actualizar', methods=['GET', 'POST'])
def apiMensajesActualizar():
    try:
        controlador = MensajesController()
        respuesta = controlador.ActualizarMensaje(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        mensaje = 'Error en la aplicacion'
        return error.Render(mensaje)


@bp.route('/api/usuarios/todos', methods=['GET', 'POST'])
def apiVerUsuarios():
    try:
        print(request)
        controlador = UsuariosController()
        respuesta = controlador.VerUsuarios(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        mensaje = 'Error en la aplicacion'
        return error.Render(mensaje)

@bp.route('/api/usuarios/ver', methods=['GET', 'POST'])
def apiVerUsuario():
    try:
        print(request)
        controlador = UsuariosController()
        respuesta = controlador.VerUsuario(request)
        print(respuesta)
        return respuesta
    except Exception as e:
        error = ErrorView()
        mensaje = 'Error en la aplicacion'
        return error.Render(mensaje)










@bp.route('/api/mensajes/actualizarlos', methods=['GET', 'POST'])
def apiActualizar():
    if request.method == 'POST':
        id = request.form['id']
        id_emisor = request.form['id_emisor']
        id_receptor = request.form['id_receptor']
        titulo = request.form['titulo']
        mensaje = request.form['mensaje']
        enviado = request.form['enviado']
        recibido = request.form['recibido']
        error = None

        if not titulo:
            error = 'El Titulo es requerido.'
        if not mensaje:
            error = 'El mensaje es requerido'
        if error is not None:
            respuesta = ('error', error)
            return jsonify(respuesta)
        else:
            db = get_db()
            db.execute(
                'UPDATE mensajes SET id_emisor = ?, id_receptor = ?, titulo = ?, mensaje = ?, enviado = ?, recibido = ?'
                ' WHERE id = ?',
                (id_emisor, id_receptor, titulo, mensaje, enviado, recibido, id)
            )
            db.commit()
            return jsonify(('resultado', 'OK'))

    return jsonify(('error','Se deben de enviar los parametros necesarios'))


@bp.route('/api/mensajes/eliminarlos', methods=['GET', 'POST'])
def apiEliminar():
    if request.method == 'POST':
        id = request.form['id']
        error = None

        if not id:
            error = 'El id del mensaje es requerido.'
        if error is not None:
            respuesta = ('error', error)
            return jsonify(respuesta)
        else:
            db = get_db()
            db.execute(
                'DELETE FROM mensajes WHERE id = ?', (id,)
            )
            db.commit()
            return jsonify(('resultado', 'OK'))

    return jsonify(('error','Se deben de enviar los parametros necesarios'))

