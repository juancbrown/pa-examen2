import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db
from flaskr.Models.UsuariosModel import UsuariosModel
from flaskr.Views.ApiVerUsuariosView import ApiVerUsuariosView

class UsuariosController:

    def VerUsuarios(self, request):
        print('Controlador usuarios')
        modelo = UsuariosModel()
        usuarios = modelo.VerUsuarios()
        view = ApiVerUsuariosView()
        return view.Render(usuarios)

    def VerUsuario(self, request):
        print('Controlador usuarios')
        modelo = UsuariosModel()
        usuarios = modelo.VerUsuario(self.ObtenerParametrosVer(request))
        view = ApiVerUsuariosView()
        return view.Render(usuarios)

    def ObtenerParametrosVer(self,request):
        parametros = {
            "id_usuario": request.form['id_usuario']
        }
        return parametros