import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

from flaskr.Models.MensajeModel import MensajeModel

from flaskr.Views.ApiNuevoMensajeView import ApiNuevoMensajeView
from flaskr.Views.ApiVerMensajeView import ApiVerMensajeView
from flaskr.Views.ApiEliminarMensajeView import ApiEliminarMensajeView

class MensajesController:

    def VerMensaje(self, request):
        parametros = self.ObtenerParametrosVer(request)
        modelo = MensajeModel()
        mensaje = modelo.VerMensaje(parametros)
        view = ApiVerMensajeView()
        return view.Render(mensaje)

    def VerMensajes(self, request):
        modelo = MensajeModel()
        mensaje = modelo.VerMensajes()
        view = ApiVerMensajeView()
        return view.Render(mensaje)

    def CrearMensaje(self, request):
        parametros = self.ObtenerParametrosCrear(request)
        modelo = MensajeModel()
        nuevoMensaje = modelo.CrearMensaje(parametros)
        view = ApiNuevoMensajeView()
        return view.Render(nuevoMensaje)

    def EliminarMensaje(self,request):
        parametros = self.ObtenerParametrosEliminar(request)
        modelo = MensajeModel()
        respuesta = modelo.EliminarMensaje(parametros)
        view = ApiEliminarMensajeView()
        return view.Render(respuesta)

    def ActualizarMensaje(self,request):
        parametros = self.ObtenerParametrosActualizar(request)
        modelo = MensajeModel()
        respuesta = modelo.ActualizarMensaje(parametros)
        view = ApiVerMensajeView()
        return view.Render(respuesta)

    def VerMensajesEmisor(self,request):
        parametros = self.ObtenerParametrosEmisor(request)
        modelo = MensajeModel()
        mensaje = modelo.MensajesEmisor(parametros)
        view = ApiVerMensajeView()
        return view.Render(mensaje)

    def VerMensajesReceptor(self,request):
        parametros = self.ObtenerParametrosReceptor(request)
        modelo = MensajeModel()
        mensaje = modelo.MensajesReceptor(parametros)
        view = ApiVerMensajeView()
        return view.Render(mensaje)

    def VerBitacoraMensaje(self, request):
        pass

    def ValidarParametrosCrear(self, parametros):
        #Validar campos
        return True

    def ObtenerParametrosCrear(self,request):
        parametros = {
            "id_emisor": request.form['id_emisor'],
            "id_receptor": request.form['id_receptor'],
            "titulo": request.form['titulo'],
            "mensaje": request.form['mensaje'],
            "enviado": request.form['enviado'],
            "recibido": request.form['recibido']
        }
        if self.ValidarParametrosCrear(parametros):
            return parametros
        else:
            return parametros

        raise Exception("Error de parametros")

    def ObtenerParametrosVer(self,request):
        parametros = {
            "id_mensaje": request.form['id_mensaje']
        }
        return parametros

    def ObtenerParametrosEmisor(self, request):
        parametros = {
            "id_emisor":  request.form['id_emisor']
        }
        return parametros

    def ObtenerParametrosReceptor(self, request):
        parametros = {
            "id_receptor": request.form['id_receptor']
        }
        return parametros


    def ObtenerParametrosEliminar(self, request):
        parametros = {
            "id_eliminar": request.form['id_eliminar']
        }
        return parametros

    def ObtenerParametrosActualizar(self, request):
        parametros = {
            "id": request.form['id'],
            "id_emisor": request.form['id_emisor'],
            "id_receptor": request.form['id_receptor'],
            "titulo": request.form['titulo'],
            "mensaje": request.form['mensaje'],
            "enviado": request.form['enviado'],
            "recibido": request.form['recibido']
        }
        return parametros