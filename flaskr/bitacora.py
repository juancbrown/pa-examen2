from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('bitacora', __name__)


@bp.route('/bitacora/')
def index():
    db = get_db()
    mensajes = db.execute(
        'select m.id, m.id_emisor, e.username, m.id_receptor, r.username, m.titulo, m.mensaje, m.enviado, m.recibido '
        'from mensajes m join user e where e.id = m.id_emisor join user r on r.id = m.id_receptor'
    ).fetchall()
    return render_template('mensaje/index.html', mensajes=mensajes)



@bp.route('/api/bitacora/todos')
def apiIndex():
    db = get_db()
    mensajes = db.execute(
        'select m.id, m.id_emisor, e.username, m.id_receptor, m.titulo, m.mensaje, m.enviado, m.recibido '
        'from mensajes m join user e where e.id = m.id_emisor'
    ).fetchall()
    return jsonify(mensajes)

@bp.route('/api/bitacora/nuevo', methods=['GET', 'POST'])
def apiCrear():
    if request.method == 'POST':
        id_emisor = request.form['id_emisor']
        id_receptor = request.form['id_receptor']
        titulo = request.form['titulo']
        mensaje = request.form['mensaje']
        enviado = request.form['enviado']
        recibido = request.form['recibido']
        error = None

        if not titulo:
            error = 'El Titulo es requerido.'
        if not mensaje:
            error = 'El mensaje es requerido'
        if error is not None:
            respuesta = ('error', error)
            return jsonify(respuesta)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO mensajes (id_emisor, id_receptor, titulo, mensaje, enviado, recibido)'
                ' VALUES (?, ?, ?, ?, ?, ?)',
                (id_emisor, id_receptor, titulo, mensaje, enviado, recibido)
            )
            db.commit()
            return jsonify(('resultado', 'OK'))

    return jsonify(('error','Se deben de enviar los parametros necesarios'))

@bp.route('/api/bitacora/actualizar', methods=['GET', 'POST'])
def apiActualizar():
    if request.method == 'POST':
        id = request.form['id']
        id_emisor = request.form['id_emisor']
        id_receptor = request.form['id_receptor']
        titulo = request.form['titulo']
        mensaje = request.form['mensaje']
        enviado = request.form['enviado']
        recibido = request.form['recibido']
        error = None

        if not titulo:
            error = 'El Titulo es requerido.'
        if not mensaje:
            error = 'El mensaje es requerido'
        if error is not None:
            respuesta = ('error', error)
            return jsonify(respuesta)
        else:
            db = get_db()
            db.execute(
                'UPDATE mensajes SET id_emisor = ?, id_receptor = ?, titulo = ?, mensaje = ?, enviado = ?, recibido = ?'
                ' WHERE id = ?',
                (id_emisor, id_receptor, titulo, mensaje, enviado, recibido, id)
            )
            db.commit()
            return jsonify(('resultado', 'OK'))

    return jsonify(('error','Se deben de enviar los parametros necesarios'))


@bp.route('/api/bitacora/eliminar', methods=['GET', 'POST'])
def apiEliminar():
    if request.method == 'POST':
        id = request.form['id']
        error = None

        if not id:
            error = 'El id del mensaje es requerido.'
        if error is not None:
            respuesta = ('error', error)
            return jsonify(respuesta)
        else:
            db = get_db()
            db.execute(
                'DELETE FROM mensajes WHERE id = ?', (id,)
            )
            db.commit()
            return jsonify(('resultado', 'OK'))

    return jsonify(('error','Se deben de enviar los parametros necesarios'))


@bp.route('/api/bitacora/ver', methods=['GET', 'POST'])
def apiVer():
    if request.method == 'POST':
        id = request.form['id']
        error = None

        if not id:
            error = 'El id del mensaje es requerido.'
        if error is not None:
            respuesta = ('error', error)
            return jsonify(respuesta)
        else:
            db = get_db()
            mensajes = db.execute(
                'SELECT * FROM mensajes WHERE id = ?', (id,)
            ).fetchone()
            return jsonify(mensajes)

    return jsonify(('error','Se deben de enviar los parametros necesarios'))

