import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

from flaskr.Models.MensajeModel import MensajeModel

class ApiNuevoMensajeView:

    def Render(self, mensaje):
        respuesta = ('OK', 'Los datos se han guardado satisfactoriamente')
        return jsonify(respuesta)