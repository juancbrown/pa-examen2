import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

from flaskr.Models.MensajeModel import MensajeModel

class ApiVerMensajeView:

    def Render(self, mensajes):
        datos = []
        print('Excelente printeando usuarios')
        for mensaje in mensajes:
            datos.append({
                'id': mensaje[0],
                'id_emisor': mensaje[1],
                'id_receptor': mensaje[2],
                'titulo': mensaje[3],
                'mensaje': mensaje[4],
                'enviado': mensaje[5],
                'recibido': mensaje[6]
            })
        return jsonify(datos)

